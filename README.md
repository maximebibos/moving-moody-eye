# Mad Eye Moody moving props

##Tutoriel suivi
(simplifié adapté à mon cas)
https://www.instructables.com/id/Mad-Eye-Moody-Moving-Eyeball-Prop/

(original oeil bidirectionnel)
https://learn.adafruit.com/3d-printed-bionic-eye/overview

##Assets et logiciel
* Adafruit Trinket 5V
* Battery holder 3xAAA 1,5V
* Servo MG90S
* IDE Arduino 1.8.8 for Windows
* Type de carte selectionnée dans le logiciel : Adafruit trinket ATtiny85 @16MHz

## Code
* **moving-eye.edt**  : 
Le code qui est uploader sur la carte.


* **console-out.txt** : 
Le retour de la console lors du build du projet avant de le téléverser

* **Adafruit_SorftServo-master** : 
La librairie utilisé par *moving-eye.edt*, elle est importée dans la conf de l'IDE Arduino

## Pictures
Des photos pour aider : 
* **schema_branchement.jpg** : le schéma que j'ai suivi pour souder les composants
* **montage_focus_branchement.jpg** : photo du montage réel en mettant de côté les longuers de fils
* **montage_global.jpg** : une vue d'ensemble du montage, désolé si c'est brouillon... :/
* **servo.jpg** : photo du servo utilisé
* **servo2.jpg** : photo du bas du servo avec les fils
* **soudure_carte.jpg** : vu de comment sont soudé les fils sur la carte Adafruit

##Notes
Pour des raisosn de praticité j'ai utilisé l'IDE Arduino sur Windows car sous Linux c'était la croix et la bannière 
pour tout configurer et je pensais aller plus vite sur Windows.

Pour notre entrevue, je pourrais pas amener mon PC fixe qui est celui sur lequel j'ai bossé.

Si je téléverse sur la carte depuis l'IDE un sample de code simple qui fait clignoter la Led de la carte ça fonctionne, 
dès qu'il y a une interaction avec un asset extérieur, ca ne marche plus (problème de conf ? matériel défectueux ?)