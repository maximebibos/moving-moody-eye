#include <Arduino.h>

class ServoController {
 public:
  ServoController();
  void attach(uint8_t pin);
  // Value to write: 0 is -90°, 180 is +90°
  void write(uint8_t a);
  void tick();
 private:
  boolean isAttached;
  uint8_t pin;
  uint8_t dutyCycle;
};
