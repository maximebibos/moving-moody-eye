/*******************************************************************
  Bionic Eye sketch for Adafruit Trinket.

  by Bill Earl
  for Adafruit Industries

  Required library is the Adafruit_SoftServo library
  available at https://github.com/adafruit/Adafruit_SoftServo
  The standard Arduino IDE servo library will not work with 8 bit
  AVR microcontrollers like Trinket and Gemma due to differences
  in available timer hardware and programming. We simply refresh
  by piggy-backing on the timer0 millis() counter

  Trinket:        Bat+    Gnd       Pin #0  Pin #1
  Connection:     Servo+  Servo-    Tilt    Rotate
                  (Red)   (Brown)   Servo   Servo
                                    (Orange)(Orange)

 *******************************************************************/
#include "ServoController.h"

// Define the tilt servo pin
#define TILTSERVOPIN 0
// Define the timer depending on your arduino device.
// For uno, it should be TIMSK0
// For nano, it should be TIMSK?
#define TIMER TIMSK

ServoController controller;
uint16_t timer = 0;

void setup() {
  controller.attach(TILTSERVOPIN);   // Attach the servo to pin 0 on Trinket
  controller.write(25);                // Tell servo to go to initial position
  delay(15);                         // Wait 15ms for the servo to reach the position
}

void loop() {
  timer = millis();
  if (timer % 20 != 0) {
    return;
  }
  controller.tick();
  if (timer % 100 != 0) {
    return;
  }
  if(random(100) > 65) {
    uint8_t val = random(0, 60);
     controller.write(val);
  }
}
