#include <Arduino.h>
#include "ServoController.h"

ServoController::ServoController(void) {
  isAttached = false;
}

void ServoController::attach(uint8_t pin) {
  pinMode(pin, OUTPUT);
  this->pin = pin;
  this->dutyCycle = 0;
  isAttached = true;
}

void ServoController::write(uint8_t a) {
  if (! isAttached) return;
  dutyCycle = map(a, 0, 180, 10, 20);
}

void ServoController::tick() {
  if (! isAttached) return;
  digitalWrite(pin, HIGH);
  uint16_t timeOnUs = 200 * dutyCycle;
  delayMicroseconds(timeOnUs);
  digitalWrite(pin, LOW);
  delayMicroseconds(20000 - timeOnUs);
}
